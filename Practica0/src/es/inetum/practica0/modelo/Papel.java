package es.inetum.practica0.modelo;

public class Papel extends PiedraPapelTijeraFactory {
	public Papel() {
		this("papel", PAPEL);
	}
	public Papel(String pNom, int pNum) {
		super(pNom,pNum);
		
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum==PAPEL;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {
		case PIEDRA:
		case SPOCK:	
			resul=1;
			this.descripcionResultado ="Papel le gano a " + pPiedPapelTijera.getNombre();
			break ;
		case TIJERA:
		case LAGARTO:	
			resul=-1;
			this.descripcionResultado ="Papel perdi� con " + pPiedPapelTijera.getNombre();
			break;
		default:			
			resul=0;
			this.descripcionResultado = "Papel empata con " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}

}
