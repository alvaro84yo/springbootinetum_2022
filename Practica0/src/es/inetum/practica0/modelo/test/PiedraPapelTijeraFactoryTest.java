package es.inetum.practica0.modelo.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.*;

import es.inetum.practica0.modelo.*;

class PiedraPapelTijeraFactoryTest {
	PiedraPapelTijeraFactory piedra, papel,tijera, lagarto, spock;

	@BeforeEach
	void setUp() throws Exception {
		piedra 	= new Piedra()	;
		papel 	= new Papel()	;
		tijera 	= new Tijera()	;
		lagarto = new Lagarto()	;
		spock 	= new Spock()	;
	}

	@AfterEach
	void tearDown() throws Exception {
		piedra = null;
		papel  = null;
		tijera = null;
		lagarto = null;
		spock 	= null;
	}

	@Test
	void testGetInstancePiedra() {
		assertEquals("piedra", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PIEDRA)
													  .getNombre()
													  .toLowerCase());
		
	}
	@Test
	void testGetInstancePapel() {
		assertEquals("papel", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PAPEL)
													  .getNombre()
													  .toLowerCase());		
	}

	@Test
	void testGetInstanceTijera() {
		assertEquals("tijera", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.TIJERA)
													  .getNombre()
													  .toLowerCase());		
	}
	
	@Test
	void testGetInstanceLagarto() {
		assertEquals("lagarto", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.LAGARTO)
													  .getNombre()
													  .toLowerCase());		
	}
	@Test
	void testGetInstanceSpock() {
		assertEquals("spock", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.SPOCK)
													  .getNombre()
													  .toLowerCase());		
	}
	
	@Test
	void testCompararPiedraGanaATijera() {
		assertEquals(1, piedra.comparar(tijera));
		assertEquals("piedra le gana a tijera", piedra.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararPiedraGanaALagarto() {
		assertEquals(1, piedra.comparar(lagarto));
		assertEquals("piedra le gana a lagarto", piedra.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararPiedraPierdeConPapel() {
		assertEquals(-1, piedra.comparar(papel));
		assertEquals("piedra pierdi� con papel", piedra.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararPiedraPierdeConSpock() {
		assertEquals(-1, piedra.comparar(spock));
		assertEquals("piedra pierdi� con spock", piedra.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararPiedraEmpataConPiedra() {
		assertEquals(0, piedra.comparar(piedra));
		assertEquals("piedra empata con piedra", piedra.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararPapelGanaConPiedra() {
		assertEquals(1, papel.comparar(piedra));
		assertEquals("papel le gano a piedra", papel.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testPapelGanaSpock() {
		assertEquals(1, papel.comparar(spock));
		assertEquals("papel le gano a spock", papel.getDescripcionResultado()
				  							 	.toLowerCase());
	}
	@Test
	void testCompararPapelPierdeConTijera() {
		assertEquals(-1, papel.comparar(tijera));
		assertEquals("papel perdi� con tijera", papel.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testPapelPierdeConLagarto() {
		assertEquals(-1, papel.comparar(lagarto));
		assertEquals("papel perdi� con lagarto", papel.getDescripcionResultado()
				  							 	.toLowerCase());
	}
	@Test
	void testCompararPapelEmpataConPapel() {
		assertEquals(0, papel.comparar(papel));
		assertEquals("papel empata con papel", papel.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararTijeraGanaAPapel() {
		assertEquals(1, tijera.comparar(papel));
		assertEquals("tijera le gana a papel", tijera.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararTijeraGanaALagarto() {
		assertEquals(1, tijera.comparar(lagarto));
		assertEquals("tijera le gana a lagarto", tijera.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararTijeraPierdeConPiedra() {
		assertEquals(-1, tijera.comparar(piedra));
		assertEquals("tijera perdi� con piedra", tijera.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararTijeraPierdeConSpock() {
		assertEquals(-1, tijera.comparar(spock));
		assertEquals("tijera perdi� con spock", tijera.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararTijeraEmpataConTijera() {
		assertEquals(0, tijera.comparar(tijera));
		assertEquals("tijera empata con tijera", tijera.getDescripcionResultado()
													  .toLowerCase());
	}	
	
	@Test
	void testCompararLagartoGanaASpock() {
		assertEquals(1, lagarto.comparar(spock));
		assertEquals("lagarto le gana a spock", lagarto.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararLagartoGanaAPapel() {
		assertEquals(1, lagarto.comparar(papel));
		assertEquals("lagarto le gana a papel", lagarto.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararLagartoPierdeConTijera() {
		assertEquals(-1, lagarto.comparar(tijera));
		assertEquals("lagarto perdi� con tijera", lagarto.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararLagartoPierdeConPiedra() {
		assertEquals(-1, lagarto.comparar(piedra));
		assertEquals("lagarto perdi� con piedra", lagarto.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararLagartoEmpataConLagarto() {
		assertEquals(0, lagarto.comparar(lagarto));
		assertEquals("lagarto empata con lagarto", lagarto.getDescripcionResultado()
													  .toLowerCase());
	}	
	
	@Test
	void testCompararSpockGanaATijera() {
		assertEquals(1, spock.comparar(tijera));
		assertEquals("spock le gana a tijera", spock.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararSpockGanaATPiedra() {
		assertEquals(1, spock.comparar(piedra));
		assertEquals("spock le gana a piedra", spock.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararSpockPierdeConPapel() {
		assertEquals(-1, spock.comparar(papel));
		assertEquals("spock perdi� con papel", spock.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararSpockPierdeConLagarto() {
		assertEquals(-1, spock.comparar(lagarto));
		assertEquals("spock perdi� con lagarto", spock.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararSpockEmpataConSpock() {
		assertEquals(0, spock.comparar(spock));
		assertEquals("spock empata con spock", spock.getDescripcionResultado()
													  .toLowerCase());
	}
	
}
