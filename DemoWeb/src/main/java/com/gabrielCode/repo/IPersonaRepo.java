package com.gabrielCode.repo;

import com.gabrielCode.model.*;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPersonaRepo extends JpaRepository<Persona,Integer> {

}
