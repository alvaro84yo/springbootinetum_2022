package com.gabrielCode.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


import com.gabrielCode.model.*;

import com.gabrielCode.repo.*;

@Controller
public class DemoController {
	@Autowired
	private IUsuarioRepo usuRepo;
	private IPersonaRepo repo;
	
	@GetMapping("/greeting")
	public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {

		Persona per = new Persona(0, name);
		repo.save(per);
		usuRepo.save(new Usuario(0, "Pedro", "Sanchez"));
		
		model.addAttribute("name", name);
		
		return "greeting";
	}
	

}
