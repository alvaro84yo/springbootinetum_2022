<%@page import="java.util.List"  %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Resultado del juego de Piedra Papel y Tijera</title>
</head>
<body>
<h1>Resultado del juego</h1>
<div id=encabezado>
	<h3>Has elegido jugar ${jugador.getNombre()} contra  ${computadora.getNombre() }</h3>
</div>
<div id = resultado>
	</br>
	<h2>${resultado}</h2>
</div>
</body>
</html>