package es.inetum.practica0.modelo;

public class Piedra extends PiedraPapelTijeraFactory {
	public Piedra() {
		this("piedra", PIEDRA );
	}

	public Piedra(String pNom, int pNum) {
		super(pNom, pNum);		
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum==PIEDRA;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {
		case TIJERA:
		case LAGARTO:
			resul=1;
			this.descripcionResultado = "piedra le gana a " + pPiedPapelTijera.getNombre();
			break;
			
        case PAPEL:
        case SPOCK:
			resul=-1;
			this.descripcionResultado = "piedra pierdi� con " + pPiedPapelTijera.getNombre();
			break;

		default:
			resul=0;
			this.descripcionResultado = "piedra empata con " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}
}
