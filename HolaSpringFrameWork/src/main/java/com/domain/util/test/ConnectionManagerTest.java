package com.domain.util.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import com.domain.util.ConnectionManager;

public class ConnectionManagerTest {

	//lote de pruebass
	Connection con;
	
	@BeforeEach
	public void setUp() throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/spring", "root", "1234");
		ConnectionManager.conectar();
	}

	@AfterEach
	public void tearDown() throws Exception {
		con = null;
		ConnectionManager.desconectar();
	}

	@Test
	public void testConectar() {
		
		try {
			ConnectionManager.conectar();
			assertTrue(true); //si paso lo pinto de verde
			
		} catch (ClassNotFoundException | SQLException e) {
			assertTrue(false); //si no paso lo pinto de rojo
			e.printStackTrace();
		}
	}

	@Test
	public void testDesconectar() {
		try {
			ConnectionManager.desconectar();
			assertTrue(true);
		} catch (SQLException e) {
			assertFalse(false);
			e.printStackTrace();
		}
		
	}

	@Test
	public void testGetConnection() {
		assertNotNull( ConnectionManager.getConnection());
		
	}

}
