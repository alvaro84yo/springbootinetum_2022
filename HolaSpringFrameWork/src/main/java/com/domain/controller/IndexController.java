package com.domain.controller;

import java.util.ArrayList;
import es.inetum.practica0.modelo.*;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class IndexController {
	@RequestMapping("/home")
	public String goIndex() {
		return "Index";
	}
	
	@RequestMapping("/")
	public String getPresentacion() {
		return "Presentacion";
	}
	
	@RequestMapping("/listado")
	public String goListado(Model model) {
		List<String> alumnos=new ArrayList<String>() ;
		alumnos.add("Juan");
		alumnos.add("Pedro");
		alumnos.add("Jose");
		
		model.addAttribute("titulo", "Listado de alumnos");
		model.addAttribute("profesor", "Gabriel Casas");
		model.addAttribute("alumnos", alumnos);
		return "Listado";
	}
	
	@RequestMapping(value = "/juego/{nombre}", method = RequestMethod.GET)
	public String goJuego(@PathVariable("nombre") String nombre, Model model) {		
		List<PiedraPapelTijeraFactory> opciones = new ArrayList<PiedraPapelTijeraFactory>();
		System.out.println("nombre=" + nombre);

		for(int i =1;i<6;i++)
			opciones.add(PiedraPapelTijeraFactory.getInstance(i));
			
		model.addAttribute("nombre", nombre);
		model.addAttribute("opciones", opciones);	
		return "PiedraPapelTijera";
	}
	
	@RequestMapping("/resolverJuego")
	public String goResolverJuego(@RequestParam(required = false) Integer selOpcion, Model model) {	
		System.out.println("******************** paso por /resolverJuego ********************:" + selOpcion );
	
		PiedraPapelTijeraFactory computadora = PiedraPapelTijeraFactory.getInstance((int)(Math.random()*100%5+1));
		PiedraPapelTijeraFactory jugador = PiedraPapelTijeraFactory.getInstance(selOpcion.intValue());
		
		jugador.comparar(computadora);
		
		model.addAttribute("jugador", jugador);
		model.addAttribute("computadora", computadora);
		model.addAttribute("resultado", jugador.getDescripcionResultado());
		return "Resultados";
		
	}
}
